require "rover"
describe Rover do

  describe ".make movement" do
    context "given correct parameters" do
      it "returns correct coordinates" do
        rover = Rover.new(["88", "12", "E", "MMLMRMMRRMML"])
        rover.make_movement
        expect(rover.rover_x_coord).to eq(3)
        expect(rover.rover_y_coord).to eq(3)
        expect(rover.rover_direction).to eq('S')

      end
    end

    context "given parameters and directions that exceed the surface" do
      it "will only make movements within the surface" do
        rover = Rover.new(["88", "88", "E", "MMR"])
        rover.make_movement
        expect(rover.rover_x_coord).to eq(8)
        expect(rover.rover_y_coord).to eq(8)
        expect(rover.rover_direction).to eq('S')

      end
    end

    context "given no parameter" do
      it "returns rover with no coordinates" do
        rover = Rover.new([])
        rover.make_movement
        expect(rover.rover_x_coord).to be_nil
        expect(rover.rover_y_coord).to be_nil
        expect(rover.rover_direction).to be_nil

      end
    end

  end
end
