class Rover

  attr_reader :rover_x_coord, :rover_y_coord, :rover_direction

  def initialize(args)
    if args.length == 4
      @grid_x = args[0][0].to_i
      @grid_y = args[0][1].to_i
      @rover_x_coord = args[1][0].to_i
      @rover_y_coord = args[1][1].to_i
      @rover_direction = args[2][0]
      @movements = args[3].chars
    else
      print "Please provide all parameters\n"
    end
  end

  def make_movement
    unless @movements.nil?
      @movements.each do |move|
        case move
          when "M"
            move_rover
          when "R"
            rotate_right
          when "L"
            rotate_left
          else
            puts "Movement invalid"
        end
      end
      print "The rover's final position " + @rover_x_coord.to_s + " " + @rover_y_coord.to_s + " " + @rover_direction + "\n"
    end
  end

  def move_rover
    case @rover_direction
      when "N"
        @rover_y_coord += @rover_y_coord < @grid_y ? 1 : 0
      when "E"
        @rover_x_coord += @rover_x_coord < @grid_x ? 1 : 0
      when "S"
        @rover_y_coord -= @rover_y_coord > 0 ? 1 : 0
      when "W"
        @rover_x_coord -= @rover_x_coord > 0 ? 1 : 0
    end
  end

  def rotate_left
    case @rover_direction
      when "N"
        @rover_direction = "W"
      when "E"
        @rover_direction = "N"
      when "S"
        @rover_direction = "E"
      when "W"
        @rover_direction = "S"
    end
  end

  def rotate_right
    case @rover_direction
      when "N"
        @rover_direction = "E"
      when "E"
        @rover_direction = "S"
      when "S"
        @rover_direction = "W"
      when "W"
        @rover_direction = "N"
    end
  end
end

rover = Rover.new(ARGV)
rover.make_movement
