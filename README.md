# README #

I decided to make a class for the Rover so that the variables associated with it could be assigned. After creating the class, created an instance of it initialising the variables and then executing the movements of the Rover

The case statements in place make sure that all movements fall within Mar's surface, if a move will make the rover go over, the move will not be done but the rotation will still be done

There are tests setup to check movements that go out of bounds and those that don't

Here is an example of how to run the program from the command line in the app directory

ruby lib/rover.rb 88 12 E MMLMRMMRRMML
